#!/usr/bin/env bash

set -eux

CONTEXT="${CONTEXT:-microk8s}"
echo "$CONTEXT"

if [[ "$CONTEXT" == "microk8s" ]]; then
    microk8s enable dns storage registry metallb:10.64.140.43-10.64.140.49
    microk8s kubectl wait --for=condition=available deployment --all -A --timeout=10m
fi

# Our stuff
env KUBECONFIG="/home/knkski/.kube/$CONTEXT.config" kubectl --context="$CONTEXT" apply --kustomize ./"$CONTEXT"/ --server-side=true ||
    env KUBECONFIG="/home/knkski/.kube/$CONTEXT.config" kubectl --context="$CONTEXT" apply --kustomize ./"$CONTEXT"/ --server-side=true

env KUBECONFIG="/home/knkski/.kube/$CONTEXT.config" kubectl --context="$CONTEXT" wait --for=condition=available deployment --all -A --timeout=10m
env KUBECONFIG="/home/knkski/.kube/$CONTEXT.config" kubectl wait -ncocoa -lstatefulset.kubernetes.io/pod-name=cocoa-db-1-0 --for=condition=ready pod --timeout=10m

if [[ "$CONTEXT" == "microk8s" ]]; then
    NAME="$(microk8s kubectl get secret -ncocoa --sort-by=.metadata.creationTimestamp --output=jsonpath='{.items..metadata.name}')"
    NAME="$(echo "$NAME" | tr " " "\n" | grep 'cocoa-db-secret' | tail -n1)"
    PASS="$(microk8s kubectl get secret -ncocoa "$NAME" --template='{{index .data "cocoa-user"}}' | base64 --decode)"
    SUPERPASS="$(microk8s kubectl get secret -ncocoa "$NAME" --template='{{index .data "cocoa-user"}}' | base64 --decode)"
    kubectl exec -itncocoa cocoa-db-1-0 -- psql postgres://postgres:"$SUPERPASS"@localhost <<EOF
    CREATE DATABASE cocoa_db;
    CREATE USER cocoa_user WITH PASSWORD '$PASS';
    GRANT ALL PRIVILEGES ON DATABASE cocoa_db TO cocoa_user;

    \connect cocoa_db;

    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to cocoa_user;
    GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public to cocoa_user;
    GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public to cocoa_user;
EOF

    poetry run python manage.py migrate
    poetry run python manage.py shell -c "from convention.models import Convention; Convention.objects.get()" >/dev/null 2>&1 ||
        poetry run python manage.py generate_test_data
fi
