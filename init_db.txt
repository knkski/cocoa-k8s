kubectl exec -itncocoa cocoa-db-1-0 -- psql -U postgres
CREATE DATABASE cocoa_db;
CREATE USER cocoa_user WITH PASSWORD ';
GRANT ALL PRIVILEGES ON DATABASE cocoa_db TO cocoa_user;

kubectl exec -itncocoa cocoa-db-1-0 -- psql -U postgres -d cocoa_db
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to cocoa_user;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public to cocoa_user;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public to cocoa_user;

kubectl exec -itncocoa cocoa-db-1-0 -- pg_dump postgresql://cocoa_user:'@cocoa-db/cocoa_db | gzip > "backups/cocoa-"(date '+%Y%m%d-%H%M%S')".sql.gz"
gzip -dc backups/$BACKUP | kubectl exec -itncocoa cocoa-db-1-0 -- psql postgresql://cocoa_user:'@localhost/cocoa_db
