from base64 import b64encode
from pathlib import Path
from subprocess import DEVNULL, check_call
from tempfile import TemporaryDirectory
import json
# import yaml


def run(*args):
    check_call(args, stdout=DEVNULL, stderr=DEVNULL)


def gen_certs():
    namespace = "projectcontour"
    service_name = "envoy"
    asdf = "10.64.140.43.nip.io"

    with TemporaryDirectory() as td:
        td = Path(td)
        Path(td / "ssl.conf").write_text(
            f"""[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn
[ dn ]
C = US
ST = Example
L = Example
O = Example
OU = Example
CN = {asdf}
[ req_ext ]
subjectAltName = @alt_names
[ alt_names ]
DNS.1 = {service_name}
DNS.2 = {service_name}.{namespace}
DNS.3 = {service_name}.{namespace}.svc
DNS.4 = {service_name}.{namespace}.svc.cluster
DNS.5 = {service_name}.{namespace}.svc.cluster.local
DNS.6 = {asdf}
IP.1 = 127.0.0.1
[ v3_ext ]
authorityKeyIdentifier=keyid,issuer:always
basicConstraints=CA:FALSE
keyUsage=keyEncipherment,dataEncipherment,digitalSignature
extendedKeyUsage=serverAuth,clientAuth
subjectAltName=@alt_names"""
        )

        run("openssl", "genrsa", "-out", str(td / "ca.key"), "2048")
        run("openssl", "genrsa", "-out", str(td / "server.key"), "2048")
        run(
            "openssl",
            "req",
            "-x509",
            "-new",
            "-sha256",
            "-nodes",
            "-days",
            "3650",
            "-key",
            str(td / "ca.key"),
            "-subj",
            f"/CN={asdf}",
            "-out",
            str(td / "ca.crt"),
        )
        run(
            "openssl",
            "req",
            "-new",
            "-sha256",
            "-key",
            str(td / "server.key"),
            "-out",
            str(td / "server.csr"),
            "-config",
            str(td / "ssl.conf"),
        )
        run(
            "openssl",
            "x509",
            "-req",
            "-sha256",
            "-in",
            str(td / "server.csr"),
            "-CA",
            str(td / "ca.crt"),
            "-CAkey",
            str(td / "ca.key"),
            "-CAcreateserial",
            "-out",
            str(td / "cert.pem"),
            "-days",
            "365",
            "-extensions",
            "v3_ext",
            "-extfile",
            str(td / "ssl.conf"),
        )

        return {
            name: b64encode((td / name).read_bytes()).decode("utf-8")
            for name in ["cert.pem", "server.key"]
        }


def main():
    certs = gen_certs()
    with open("example-cert.yaml", "w") as f:
        json.dump(
            {
                "kind": "Secret",
                "apiVersion": "v1",
                "metadata": {"name": "example-cert", "namespace": "projectcontour"},
                "type": "kubernetes.io/tls",
                "data": {"tls.crt": certs["cert.pem"], "tls.key": certs["server.key"]},
            },
            f,
        )


if __name__ == "__main__":
    main()
